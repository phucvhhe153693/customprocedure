<%-- 
    Document   : home
    Created on : 18-03-2021, 16:44:11
    Author     : dell
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${sessionScope.acc ne null}">
            <h1>Dang nhap thanh cong</h1>
            <h1>Hello ${sessionScope.acc.account}</h1>
            <form action="search" method="post">
                <input type="text" name="text"/><br/>
                <button type="submit">Search</button>  
            </form>
            <p>${requestScope.none}</p>
            <a href="demo2.jsp" style="text-decoration: none">Nhap ho so moi</a><br/>
            <a href="approve.jsp"style="text-decoration: none">Approve</a><br/>
            <a href="decline.jsp"style="text-decoration: none">Decline</a><br/>
            <a href="login.jsp" style="text-decoration: none">Dang xuat</a>
        </c:if>
        <c:if test="${sessionScope.acc eq null}">
            <a href="login.jsp" style="text-decoration: none">Dang nhap</a>
        </c:if>
    </body>
</html>
