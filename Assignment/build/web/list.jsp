<%-- 
    Document   : list
    Created on : 31-03-2021, 15:33:44
    Author     : dell
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            
            a{
                text-decoration: none;
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        <table>
            <tr>
                <td>ID</td>
                <td>Accept</td>
               
            </tr>
            <c:forEach items="${requestScope.list}" var="list">
                <tr>
                    <td>${list.id}</td>
                    <c:if test="${list.accept == true}">
                        <td><a href="update?id=${list.id}&acc=${false}">True</a>&nbsp;</td>
                    </c:if>
                     <c:if test="${list.accept == false}">
                        <td><a href="update?id=${list.id}&acc=${true}">False</a>&nbsp;</td>
                    </c:if>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
