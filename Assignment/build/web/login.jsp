<%-- 
    Document   : login
    Created on : 18-03-2021, 16:22:43
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Hugo 0.80.0">


        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <style>
            
        </style>
        <link href="css/login.css" rel="stylesheet" >
    </head>
    <body class="text-center">
        <main class="form-signin">
            <form action="login" method="POST">
                <img class="mb-4" src="pic_assignment/HQ.jpg" alt="" width="220" height="220"> <br/>

                <h1 class="h3 mb-3 fw-normal">VIETNAM CUSTOMS</h1> 
                <input type="text" name="username" class="form-control" placeholder="Username" required autofocus/> <br/>

                <input type="password" name="password" class="form-control" placeholder="Password" required/> <br/>
                
                <a href="signup.jsp" style="text-decoration: none">Sign up</a>
                <div class="checkbox mb-3">
                    <label>
                        <input type="checkbox" name="remember" value="remember-me"> Remember me
                    </label>
                </div>
                <p>${requestScope.w_name}</p>
                <p>${requestScope.wrong}</p>
                <button type="submit" class="btn btn-primary">Login</button>


            </form>
        </main>

    </body>
</html>
