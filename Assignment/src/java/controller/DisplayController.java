/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.AgencyDao;
import dal.DestinationDao;
import dal.GeneralDao;
import dal.RegisterDao;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Agency;
import model.Destination;
import model.General;
import model.Register;

/**
 *
 * @author dell
 */
@WebServlet(name = "DisplayController", urlPatterns = {"/display"})
public class DisplayController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
//        String text = (String) session.getAttribute("code");
        String text ="ERO141021";
        GeneralDao dao1= new GeneralDao();
        ArrayList<General> lGen = dao1.getAll(text);
        
        AgencyDao dao2 = new AgencyDao();
        ArrayList<Agency> lAg = dao2.getAll(text);
        
        RegisterDao dao3 = new RegisterDao();
        ArrayList<Register> lRes = dao3.getAll(text);
        
        DestinationDao dao4 = new DestinationDao();
        ArrayList<Destination> lDes = dao4.getAll(text);
        
        request.setAttribute("gen", lGen);
        request.setAttribute("res", lRes);
        request.setAttribute("agent", lAg);
        request.setAttribute("des", lDes);
        
        request.getRequestDispatcher("demo3.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
