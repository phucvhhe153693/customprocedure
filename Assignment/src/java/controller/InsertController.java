/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.AgencyDao;
import dal.DestinationDao;
import dal.GeneralDao;
import dal.RegisterDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Agency;
import model.Destination;
import model.General;
import model.Register;

/**
 *
 * @author dell
 */
@WebServlet(name = "InsertController", urlPatterns = {"/insert"})
public class InsertController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InsertController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InsertController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("demo2.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();

            String name = request.getParameter("name");
            String type = request.getParameter("type");
            String cap = request.getParameter("cap");

            String arrival = request.getParameter("arrival");
            String departure = request.getParameter("departure");
            String p_arrival = request.getParameter("p_arrival");
            String p_departure = request.getParameter("p_departure");
            Date d_arrival = Date.valueOf(request.getParameter("d_arrival"));
            Date d_departure = Date.valueOf(request.getParameter("d_departure"));

            String p_res = request.getParameter("p_res");
            String n_res = request.getParameter("n_res");
            Date d_res = Date.valueOf(request.getParameter("d_res"));

            String imo_num = request.getParameter("imo_num");
            String sign = request.getParameter("sign");

            String a_name = request.getParameter("a_name");
            String a_city = request.getParameter("a_city");
            String a_region = request.getParameter("a_region");
            int a_zip = Integer.parseInt(request.getParameter("a_zip"));
            String a_country = request.getParameter("a_country");
            String a_mail = request.getParameter("a_mail");
            String a_fax = request.getParameter("a_fax");

            float g_ton = Float.parseFloat(request.getParameter("g_ton"));
            float n_ton = Float.parseFloat(request.getParameter("n_ton"));

            int crew = Integer.parseInt(request.getParameter("crew"));
            int passanger = Integer.parseInt(request.getParameter("guest"));
            String pos = request.getParameter("pos");
            String pur = request.getParameter("pur");

            String des = request.getParameter("des");

            Date date = Date.valueOf(request.getParameter("date"));

            String signa = request.getParameter("signa");
            String code = (String) session.getAttribute("create_code");

            General gen = new General();
            gen.setCode(code);
            gen.setCap(cap);
            gen.setType(type);
            gen.setName(name);
            gen.setPosition(pos);
            gen.setPurpose(pur);
            gen.setDescription(des);
            gen.setDate(date);
            gen.setSignature(signa);
            gen.setImo(imo_num);
            gen.setSign(sign);
            gen.setG_ton(g_ton);
            gen.setN_ton(n_ton);
            gen.setCrew(crew);
            gen.setPassanger(passanger);
            gen.setStatus("Pending");

            Destination d = new Destination();
            d.setArrival(arrival);
            d.setDeparture(departure);
            d.setP_arrival(p_arrival);
            d.setP_departure(p_departure);
            d.setD_arrival(d_arrival);
            d.setD_departure(d_departure);
            d.setCode(code);

            Register r = new Register();
            r.setP_register(p_res);
            r.setNum_register(n_res);
            r.setD_register(d_res);
            r.setCode(code);

            Agency a = new Agency();
            a.setName(a_name);
            a.setCity(a_city);
            a.setRegion(a_region);
            a.setZip(a_zip);
            a.setCountry(a_country);
            a.setEmail(a_mail);
            a.setFax(a_fax);
            a.setCode(code);

            GeneralDao db1 = new GeneralDao();
            DestinationDao db2 = new DestinationDao();
            RegisterDao db3 = new RegisterDao();
            AgencyDao db4 = new AgencyDao();

            db1.add(gen);
            db2.add(d);
            db3.add(r);
            db4.add(a);

            response.getWriter().print(code);
        } catch (IOException | NumberFormatException e) {
            response.getWriter().print("E");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
