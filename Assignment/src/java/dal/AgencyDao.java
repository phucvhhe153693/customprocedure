/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import com.sun.xml.ws.tx.at.v10.types.PrepareResponse;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Agency;

/**
 *
 * @author dell
 */
public class AgencyDao extends DBContext {

    public ArrayList<Agency> getAll(String text) {
        ArrayList<Agency> agency = new ArrayList<>();
        String sql = "SELECT * from Agency WHERE code = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            
            stm.setString(1, text);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Agency a = new Agency();
                a.setCode(rs.getString("code"));
                a.setName(rs.getString("name"));
                a.setCity(rs.getString("city"));
                a.setRegion(rs.getString("region"));
                a.setZip(rs.getInt("zip"));
                a.setCountry(rs.getString("country"));
                a.setEmail(rs.getString("email"));
                a.setFax(rs.getString("fax"));

                agency.add(a);
            }
            return agency;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return agency;
    }

    public void add(Agency a) {
        String sql = "INSERT INTO Agency VALUES (?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, a.getCode());
            stm.setString(2, a.getName());
            stm.setString(3, a.getCity());
            stm.setString(4, a.getRegion());
            stm.setInt(5, a.getZip());
            stm.setString(6, a.getCountry());
            stm.setString(7, a.getEmail());
            stm.setString(8, a.getFax());

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AgencyDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    }
