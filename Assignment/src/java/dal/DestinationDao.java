/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Destination;

/**
 *
 * @author dell
 */
public class DestinationDao extends DBContext {

    public ArrayList<Destination> getAll(String text) {
        ArrayList<Destination> des = new ArrayList<>();
        String sql = "SELECT * from Destination WHERE code = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, text);
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                Destination d = new Destination();
                d.setCode(rs.getString("code"));
                d.setArrival(rs.getString("arrival"));
                d.setDeparture(rs.getString("departure"));
                d.setP_arrival(rs.getString("p_arrival"));
                d.setP_departure(rs.getString("p_departure"));
                d.setD_arrival(rs.getDate("d_arrival"));
                d.setD_departure(rs.getDate("d_departure"));

                des.add(d);
            }
            return des;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return des;
    }

    public void add(Destination d) {
        String sql = "INSERT INTO Destination VALUES (?,?,?,?,?,?,?)";

        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, d.getCode());
            stm.setString(2, d.getArrival());
            stm.setString(3, d.getDeparture());
            stm.setString(4, d.getP_arrival());
            stm.setString(5, d.getP_departure());
            stm.setDate(6, d.getD_arrival());
            stm.setDate(7, d.getD_departure());

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AgencyDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String[] args) {
        Destination d = new Destination();
        d.setArrival("abc");
        d.setCode("abc");
        d.setDeparture("abc");
        d.setP_arrival("abc");
        d.setP_departure("abc");
        d.setD_arrival(Date.valueOf("2011-02-12"));
        d.setD_departure(Date.valueOf("2011-02-12"));
        
        new DestinationDao().add(d);
    }
}
