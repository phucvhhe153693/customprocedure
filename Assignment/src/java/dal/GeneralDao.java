/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.General;

/**
 *
 * @author dell
 */
public class GeneralDao extends DBContext {

    public ArrayList<General> getAll(String text) {
        ArrayList<General> gen = new ArrayList<>();
        String sql = "SELECT * from General WHERE code = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, text);
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                General g = new General();
                
                g.setName(rs.getString("name"));
                g.setType(rs.getString("type"));
                g.setCap(rs.getString("cap"));
                g.setImo(rs.getString("imo"));
                g.setSign(rs.getString("sign"));
                g.setG_ton(rs.getFloat("g_ton"));
                g.setN_ton(rs.getFloat("n_ton"));
                g.setCrew(rs.getInt("crew"));
                g.setPassanger(rs.getInt("passanger"));
                g.setPosition(rs.getString("position"));
                g.setPurpose(rs.getString("purpose"));
                g.setDescription(rs.getString("description"));
                g.setDate(rs.getDate("date"));
                g.setSignature(rs.getString("signature"));
                g.setCode(rs.getString("code"));
                g.setStatus(rs.getString("status"));

                gen.add(g);
            }
            return gen;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return gen;
    }

    public void add(General g) {
        String sql = "INSERT INTO General VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            
            stm.setString(1, g.getName());
            stm.setString(2, g.getType());
            stm.setString(3, g.getCap());
            stm.setString(4, g.getImo());
            stm.setString(5, g.getSign());
            stm.setFloat(6, g.getG_ton());
            stm.setFloat(7, g.getN_ton());
            stm.setInt(8, g.getCrew());
            stm.setInt(9, g.getPassanger());
            stm.setString(10, g.getPosition());
            stm.setString(11, g.getPurpose());
            stm.setString(12, g.getDescription());
            stm.setDate(13, g.getDate());
            stm.setString(14, g.getSignature());
            stm.setString(15, g.getCode());
            stm.setString(16, g.getStatus());

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AgencyDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean approve(String text){
        String sql ="UPDATE General SET status = 'Approve' WHERE CODE = ?";
         try{
             PreparedStatement stm = connection.prepareCall(sql);
             stm.setString(1, text);
             stm.executeUpdate();
             return true;
         } catch (SQLException ex) {
            Logger.getLogger(GeneralDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean decline(String text){
        String sql ="UPDATE General SET status = 'Declined' WHERE CODE = ?";
         try{
             PreparedStatement stm = connection.prepareCall(sql);
             stm.setString(1, text);
             stm.executeUpdate();
             return true;
         } catch (SQLException ex) {
            Logger.getLogger(GeneralDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static void main(String[] args) {
        GeneralDao db = new GeneralDao();
        System.out.println(db.getAll("ERO141021"));
    }
}
