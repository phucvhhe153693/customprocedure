package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Record;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HIEU19
 */
public class RecordsDao  extends DBContext{
    public ArrayList<Record> getAll(){
        ArrayList<Record> list = new ArrayList<>();
        try {
            
            String sql = "SELECT * from Records";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Record r = new Record();
                r.setId(rs.getInt("id"));
                r.setAccept(rs.getBoolean("accept"));
                
                list.add(r);
            }
        } catch (SQLException e) {
        }
        return list;
    }
    
    
    public void update(boolean x, int id){
        try {
            String sql = "update records set accept = ? where id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setBoolean(1, x);
            stm.setInt(2, id);
            stm.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    
    public static void main(String[] args) {
        System.out.println(new RecordsDao().getAll());
    }
}