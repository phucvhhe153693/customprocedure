/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Register;

/**
 *
 * @author dell
 */
public class RegisterDao extends DBContext {

    public ArrayList<Register> getAll(String text) {
        ArrayList<Register> reg = new ArrayList<>();
        String sql = "SELECT * from Register WHERE code = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            
            stm.setString(1, text);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Register r = new Register();
                r.setCode(rs.getString("code"));
                r.setP_register(rs.getString("p_register"));
                r.setNum_register(rs.getString("num_register"));
                r.setD_register(rs.getDate("d_register"));

                reg.add(r);
            }
            return reg;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reg;
    }

    public void add(Register r) {
        String sql = "INSERT INTO Register VALUES (?,?,?,?)";

        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, r.getCode());
            stm.setString(2, r.getP_register());
            stm.setString(3, r.getNum_register());
            stm.setDate(4, r.getD_register());

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AgencyDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
