/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author dell
 */
public class Destination {
    private String code;
    private String arrival;
    private String departure;
    private String p_arrival;
    private String p_departure;
    private Date d_arrival;
    private Date d_departure;

    public Destination() {
    }

    public Destination(String code, String arrival, String departure, String p_arrival, String p_departure, Date d_arrival, Date d_departure) {
        this.code = code;
        this.arrival = arrival;
        this.departure = departure;
        this.p_arrival = p_arrival;
        this.p_departure = p_departure;
        this.d_arrival = d_arrival;
        this.d_departure = d_departure;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getP_arrival() {
        return p_arrival;
    }

    public void setP_arrival(String p_arrival) {
        this.p_arrival = p_arrival;
    }

    public String getP_departure() {
        return p_departure;
    }

    public void setP_departure(String p_departure) {
        this.p_departure = p_departure;
    }

    public Date getD_arrival() {
        return d_arrival;
    }

    public void setD_arrival(Date d_arrival) {
        this.d_arrival = d_arrival;
    }

    public Date getD_departure() {
        return d_departure;
    }

    public void setD_departure(Date d_departure) {
        this.d_departure = d_departure;
    }
    
    
}
