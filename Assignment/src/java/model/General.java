/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author dell
 */
public class General {
    private int id;
    private String name;
    private String type;
    private String cap;
    private String imo;
    private String sign;
    private float g_ton;
    private float n_ton;
    private int crew;
    private int passanger;
    private String position;
    private String purpose;
    private String description;
    private Date date;
    private String signature;
    private String code;
    private String status;

    public General() {
    }

    public General(int id, String name, String type, String cap, String imo, String sign, float g_ton, float n_ton, int crew, int passanger, String position, String purpose, String description, Date date, String signature, String code, String status) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.cap = cap;
        this.imo = imo;
        this.sign = sign;
        this.g_ton = g_ton;
        this.n_ton = n_ton;
        this.crew = crew;
        this.passanger = passanger;
        this.position = position;
        this.purpose = purpose;
        this.description = description;
        this.date = date;
        this.signature = signature;
        this.code = code;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getImo() {
        return imo;
    }

    public void setImo(String imo) {
        this.imo = imo;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public float getG_ton() {
        return g_ton;
    }

    public void setG_ton(float g_ton) {
        this.g_ton = g_ton;
    }

    public float getN_ton() {
        return n_ton;
    }

    public void setN_ton(float n_ton) {
        this.n_ton = n_ton;
    }

    public int getCrew() {
        return crew;
    }

    public void setCrew(int crew) {
        this.crew = crew;
    }

    public int getPassanger() {
        return passanger;
    }

    public void setPassanger(int passanger) {
        this.passanger = passanger;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
    
    
    
    
}
