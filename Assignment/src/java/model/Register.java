/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author dell
 */
public class Register {
    private String code;
    private String p_register;
    private String num_register;
    private Date d_register;

    public Register() {
    }

    public Register(String code, String p_register, String num_register, Date d_register) {
        this.code = code;
        this.p_register = p_register;
        this.num_register = num_register;
        this.d_register = d_register;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getP_register() {
        return p_register;
    }

    public void setP_register(String p_register) {
        this.p_register = p_register;
    }

    public String getNum_register() {
        return num_register;
    }

    public void setNum_register(String num_register) {
        this.num_register = num_register;
    }

    public Date getD_register() {
        return d_register;
    }

    public void setD_register(Date d_register) {
        this.d_register = d_register;
    }
    
    
}
