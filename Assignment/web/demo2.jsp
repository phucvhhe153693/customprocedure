<!DOCTYPE html>
<html>
    <head>
        <title>General Application Form</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <style>
            html, body {
                min-height: 100%;
            }
            body, div, form, input, select, textarea, p { 
                padding: 0;
                margin: 0;
                outline: none;
                font-family: Roboto, Arial, sans-serif;
                font-size: 14px;
                color: #666;
                line-height: 22px;
            }
            h1 {
                position: absolute;
                margin: 0;
                font-size: 32px;
                color: #fff;
                z-index: 2;
            }
            h5 {
                margin: 10px 0;
            }
            .testbox {
                display: flex;
                justify-content: center;
                align-items: center;
                height: inherit;
                padding: 20px;
            }
            form {
                width: 100%;
                padding: 20px;
                border-radius: 6px;
                background: #fff;
                box-shadow: 0 0 20px 0 #095484; 
            }
            .banner {
                position: relative;
                height: 210px;
                background-image: url("/uploads/media/default/0001/01/c43630149befa5c9559813f72e99bcb6bf149e62.jpeg");  
                background-size: cover;
                display: flex;
                justify-content: center;
                align-items: center;
                text-align: center;
            }
            .banner::after {
                content: "";
                background-color: rgba(0, 0, 0, 0.5); 
                position: absolute;
                width: 100%;
                height: 100%;
            }
            input, select, textarea {
                margin-bottom: 10px;
                border: 1px solid #ccc;
                border-radius: 3px;
            }
            input {
                width: calc(100% - 10px);
                padding: 5px;
            }
            select {
                width: 100%;
                padding: 7px 0;
                background: transparent;
            }
            textarea {
                width: calc(100% - 12px);
                padding: 5px;
            }
            .item:hover p, .item:hover i, .question:hover p, .question label:hover, input:hover::placeholder, a {
                color: #095484;
            }
            .item input:hover, .item select:hover, .item textarea:hover {
                border: 1px solid transparent;
                box-shadow: 0 0 6px 0 #095484;
                color: #095484;
            }
            .item {
                position: relative;
                margin: 10px 0;
            }
            input[type="date"]::-webkit-inner-spin-button {
                display: none;
            }
            .item i, input[type="date"]::-webkit-calendar-picker-indicator {
                position: absolute;
                font-size: 20px;
                color: #a9a9a9;
            }
            .item i {
                right: 2%;
                top: 30px;
                z-index: 1;
            }
            [type="date"]::-webkit-calendar-picker-indicator {
                right: 1%;
                z-index: 2;
                opacity: 0;
                cursor: pointer;
            }
            input[type=radio], input[type=checkbox]  {
                display: none;
            }
            label.radio, label.check {
                position: relative;
                display: inline-block;
                margin: 5px 20px 15px 0;
                cursor: pointer;
            }
            .question span {
                margin-left: 30px;
            }
            span.required {
                margin-left: 0;
                color: red;
            }
            .checkbox-item label {
                margin: 5px 20px 10px 0;
            }
            label.radio:before, label.check:before {
                content: "";
                position: absolute;
                left: 0;
            }
            label.radio:before {
                width: 17px;
                height: 17px;
                border-radius: 50%;
                border: 2px solid #095484;
            }
            label.check:before {
                top: 2px;
                width: 16px;
                height: 16px;
                border-radius: 2px;
                border: 1px solid #095484;
            }
            input[type=checkbox]:checked + .check:before {
                background: #095484;
            }
            label.radio:after {
                left: 5px;
                border: 3px solid #095484;
            }
            label.check:after {
                left: 4px;
                border: 3px solid #fff;
            }
            label.radio:after, label.check:after {
                content: "";
                position: absolute;
                top: 6px;
                width: 8px;
                height: 4px;
                background: transparent;
                border-top: none;
                border-right: none;
                transform: rotate(-45deg);
                opacity: 0;
            }
            input[type=radio]:checked + label:after, input[type=checkbox]:checked + label:after {
                opacity: 1;
            }
            .btn-block {
                margin-top: 10px;
                text-align: center;
            }
            button {
                width: 150px;
                padding: 10px;
                border: none;
                border-radius: 5px; 
                background: #095484;
                font-size: 16px;
                color: #fff;
                cursor: pointer;
            }
            button:hover {
                background: #0666a3;
            }
            @media (min-width: 568px) {
                .city-item {
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: space-between;
                }
                .city-item input {
                    width: calc(50% - 20px);
                }
                .city-item select {
                    width: calc(50% - 8px);
                }
            }
            @media (min-width: 568px) {
                .arrival-item {
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: space-between;
                }
                .arrival-item input {
                    width: calc(50% - 20px);
                }
                .arrival-item select {
                    width: calc(50% - 8px);
                }
            }
            @media (min-width: 568px) {
                .a-item {
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: space-between;
                }
                .a-item input {
                    width: calc(80% - 40px);
                    padding-right: 10px;
                }
                .a-item select {
                    width: calc(60% - 8px);
                }
            }

        </style>
    </head>
    <body>
        <div class="testbox">
            <form action="insert" method="post">
                <div class="banner">
                    <h1>General Declaration</h1>
                </div>
                <div class="item">
                    <input type="text" name="create_code" value="${sessionScope.create_code}"/>
                    <a type="button"  href="create">Generate</a>
                </div>
                <div class="item">
                    <h3>Name and type of ship <span class="required">*</span></h3>
                    <div class="arrival-item">
                        <input type="text" name="name" placeholder="Name" required/>
                        <input type="text" name="type" placeholder="Type" required/>
                    </div>
                </div>

                <div class="item">
                    <h3>Captain's Full Name<span class="required">*</span></h3>
                    <input type="text" name="cap" placeholder="Mr/Mrs..."required/>
                </div>
                <div class="item">
                    <h3>Arrival and Departure<span class="required">*</span></h3>
                    <div class="arrival-item">
                        <input type="text" name="arrival" placeholder="Arrival" required/>
                        <input type="text" name="departure" placeholder="Departure" required/>
                    </div>
                </div>

                <div class="item">
                    <h3>Port of arrival/departure<span class="required">*</span></h3>
                    <div class="arrival-item">
                        <input type="text" name="p_arrival" placeholder="Port of arrival" required/>
                        <input type="text" name="p_departure" placeholder="Port of departure" required/>
                    </div>
                </div>

                <div class="item">
                    <h3>Date - Time of arrival/departure<span class="required">*</span></h3>
                    <div class="arrival-item">
                        <input type="date" name="d_arrival" placeholder="dd-MM-yyyy" required/>
                        <input type="date" name="d_departure" placeholder="dd-MM-yyyy" required/>
                    </div>
                </div>

                <div class="item">
                    <h3>Certificate of registry (Port, number and date) <span class="required">*</span></h3>
                    <div class="arrival-item">
                        <input type="text" name="p_res" placeholder="Port" required/>
                        <input type="text" name="n_res" placeholder="Number" required/>
                    </div>
                    <input type="date" name="d_res" placeholder="dd-MM-yyyy" required/>
                </div>

                <div class="item">
                    <div class="a-item">
                        <h3>IMO Number <span class="required">*</span></h3>
                        <input type="text" name="imo_num" placeholder="IMO Number" required/>
                    </div>
                    <div class="a-item">
                        <h3>Call Sign <span class="required">*</span></h3>
                        <input type="text" name="sign" placeholder="Call Sign" required/>
                    </div>

                </div>

                <div class="item">
                    <h3>Name and contact details of the ship agent<span class="required">*</span></h3>
                    <input type="text" name="a_name" placeholder="Name of ship agent" required/>
                    <div class="city-item">
                        <input type="text" name="a_city" placeholder="City" required/>
                        <input type="text" name="a_region" placeholder="Region" required/>
                        <input type="text" name="a_zip" placeholder="Postal / Zip code" required/>
                        <input type="text" name="a_country" placeholder="Country" required/>
                    </div>

                    <input type="email" name="a_mail" placeholder="Email"required/>
                    <input type="text" name="a_fax" placeholder="Fax"required/>
                </div>
                <div class="item">
                    <h3>Tonnage<span class="required">*</span></h3>
                    <div class="arrival-item">
                        <input type="text" name="g_ton" placeholder="Gross tonnage" required/>
                        <input type="text" name="n_ton" placeholder="Net tonnage" required/>
                    </div>
                </div>
                <div class="item">
                    <h3>Crew and passenger<span class="required">*</span></h3>
                    <div class="arrival-item">
                        <input type="text" name="crew" placeholder="Number of crew" required/>
                        <input type="text" name="guest" placeholder="Number of passenger" required/>
                    </div>
                </div>
                <div class="item">
                    <h3>Position of the ship in the port (berth or station)<span class="required">*</span></h3>
                    <input type="text" name="pos" placeholder="Position"required/>
                </div>
                <div class="item">
                    <h3>Brief particulars of voyage<span class="required">*</span></h3>
                    <input type="text" name="pur" required/>
                </div>

                <div class="item">
                    <h3>Brief description of the cargo<span class="required">*</span></h3>
                    <textarea rows="5" name="des"required></textarea>
                </div>


                <div class="item">
                    <p>Date</p>
                    <input type="date" name="date" placeholder="dd-MM-yyyy" required/>
                    <i class="fas fa-calendar-alt"></i>
                </div>
                <div class="item">
                    <p>Electronic signature<span class="required">*</span></p>
                    <textarea rows="3" required placeholder=""></textarea>
                </div>
                <div class="btn-block">
                    <button type="submit" >Send Application</button>
                </div>
            </form>

        </div>

    </body>
</html>