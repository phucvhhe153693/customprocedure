USE [Assignment]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 11/08/2021 9:59:45 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[account] [varchar](50) NULL,
	[password] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Agency]    Script Date: 11/08/2021 9:59:45 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Agency](
	[code] [varchar](50) NULL,
	[name] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[region] [varchar](50) NULL,
	[zip] [int] NULL,
	[country] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[fax] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Destination]    Script Date: 11/08/2021 9:59:45 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Destination](
	[code] [varchar](50) NULL,
	[arrival] [varchar](50) NULL,
	[departure] [varchar](50) NULL,
	[p_arrival] [varchar](50) NULL,
	[p_departure] [nchar](10) NULL,
	[d_arrival] [date] NULL,
	[d_departure] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[General]    Script Date: 11/08/2021 9:59:45 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[General](
	[name] [varchar](50) NOT NULL,
	[type] [varchar](50) NULL,
	[cap] [varchar](50) NULL,
	[imo] [varchar](50) NULL,
	[sign] [varchar](50) NULL,
	[g_ton] [float] NULL,
	[n_ton] [float] NULL,
	[crew] [int] NULL,
	[passanger] [int] NULL,
	[position] [varchar](50) NULL,
	[purpose] [varchar](50) NULL,
	[description] [varchar](200) NULL,
	[date] [date] NULL,
	[signature] [varchar](50) NULL,
	[code] [varchar](50) NULL,
	[status] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Records]    Script Date: 11/08/2021 9:59:45 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Records](
	[id] [int] NULL,
	[accept] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Register]    Script Date: 11/08/2021 9:59:45 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Register](
	[code] [varchar](50) NULL,
	[p_register] [varchar](50) NULL,
	[num_register] [varchar](50) NULL,
	[d_register] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User_Detail]    Script Date: 11/08/2021 9:59:45 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Detail](
	[name] [varchar](50) NOT NULL,
	[dob] [date] NOT NULL,
	[phone] [int] NOT NULL,
	[address] [varchar](50) NOT NULL,
	[comp_name] [nchar](10) NULL
) ON [PRIMARY]
GO
